# RGB to HSV

## Tests

There are various tests to test the ChatGPT generated functions and the Chernov functions.

- Pass `--all` to test every color in the RGB spectrum. By default the script tests a few hand-picked samples.

`python3.10 test.py correctness --chapgpt`
`python3.10 test.py correctness --all`

`python3.10 test.py time --all`
`python3.10 test.py time --chapgpt --all`

