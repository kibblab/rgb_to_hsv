import numpy as np

class Chernov():
    """
    To avoid rounding point errors, this module implements the RGB to HSV conversion algorithm as presented in
    "Integer-based accurate conversion between RGB and HSV color spaces"
    by Vladimir Chernov, Jarmo Alander, Vladimir Bochko

    Source: https://bitbucket.org/chernov/colormath_hsv/src
    """

    E = int(65537)
    S_MAX = int(65535)
    H_MAX = int(393222)

    def rgb_to_hsv(r: int, g: int, b: int) -> tuple[ int, int, int]:
        """Convert RGB to an integer representation of HSV."""
        k=[r,g,b]
        k.sort()
        mn, mid, mx = k

        v = mx
        d = mx - mn

        if d == 0:
            return 0, 0, v
        
        if mx == r and mn == b:
            i=0
        elif mx == g and mn == b:
            i=1
        elif mx == g and mn == r:
            i=2
        elif mx == b and mn == r:
            i=3
        elif mx == b and mn == g:
            i=4
        else:
            i=5

        s = ((d << 16) - 1) // v

        f: int = (((mid - mn) << 16) // d) + 1

        if i == 1 or i == 3 or i == 5:
            f = Chernov.E - f

        h = (Chernov.E * i) + f
        return h, s, v

    def hsv_to_rgb(h: int, s: int, v: int):
        """Convert integer representation of HSV to RGB."""
        if s == 0 or v == 0:
            return v, v, v
        
        d = ((s * v) >> 16) + 1
        mn = v - d

        if h < Chernov.E:
            i=0
        elif h >= Chernov.E and h < 2 * Chernov.E:
            i=1
        elif h >= 2 * Chernov.E and h < 3 * Chernov.E:
            i=2
        elif h >= 3 * Chernov.E and h < 4 * Chernov.E:
            i=3
        elif h >= 4 * Chernov.E and h < 5 * Chernov.E:
            i=4
        else:
            i=5

        if i == 1 or i == 3 or i == 5:
            f = Chernov.E * (i + 1) - h
        else:
            f = h - (Chernov.E * i)

        mid = ((d * f) >> 16) + mn

        if i == 0:
            return v, mid, mn
        elif i == 1:
            return mid, v, mn
        elif i == 2:
            return mn, v, mid
        elif i == 3:
            return mn, mid, v
        elif i == 4:
            return mid, mn, v
        else:
            return v, mn, mid

    def rgb_to_hsv_degrees(r: int, g: int, b: int) -> tuple[ int, int, int]:
        """Convert RGB to an integer representation of HSV."""
        k=[r,g,b]
        k.sort()
        mn, mid, mx = k

        v = mx
        d = mx - mn

        if d == 0:
            return 0, 0, v * 100 // 255
        
        if mx == r and mn == b:
            i=0
        elif mx == g and mn == b:
            i=1
        elif mx == g and mn == r:
            i=2
        elif mx == b and mn == r:
            i=3
        elif mx == b and mn == g:
            i=4
        else:
            i=5

        s = ((d << 16) - 1) // v

        f: int = (((mid - mn) << 16) // d) + 1

        if i == 1 or i == 3 or i == 5:
            f = Chernov.E - f

        h = (Chernov.E * i) + f
        return h * 360 // Chernov.H_MAX, s * 100 // Chernov.S_MAX , v * 100 // 255

# =======================================================================================

class Helper():

    def hex_to_rgb(n: int):
        """Converts the integer representation of a hex into RGB"""
        s=f"{n:06x}"
        return tuple(int(s[i:i+2], 16)  for i in (0, 2, 4))

    def rgb_to_hex(rgb: tuple[int, int, int]):
        return f'{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}'

    def hsv_parametric(int_hsv: tuple[int, int, int]):
        """Convert an integer HSV into floating point over the range [0, 1]"""
        return ( int_hsv[0] / Chernov.H_MAX, int_hsv[1] / Chernov.S_MAX, int_hsv[2] / 255 )

    def hsv_360(int_hsv: tuple[int, int, int]):
        """Convert integer HSV into human-friendly values. Hue in degrees, saturation and value as 0-100"""
        return ( ((int_hsv[0] * 360 // Chernov.H_MAX)  + 360) % 360, int_hsv[1] * 100 // Chernov.S_MAX, int_hsv[2] * 100 // 255)

    def as_hsv_int(hsv_360: list[int]):
        """Convert a human-friendly HSV into the integer representation"""
        hue = ((hsv_360[0]) + 360) % 360 # within 0 <= x < 360
        return [ Chernov.H_MAX * hue // 360, Chernov.S_MAX * hsv_360[1] // 100, 255 * hsv_360[2] // 100 ]

# ======================================================================================

class ChatGPT():

    def rgb_to_hsv(r, g, b):

        # Find the maximum and minimum values among R, G, and B
        max_value = max(r, g, b)
        min_value = min(r, g, b)
        delta = max_value - min_value

        # Compute the hue value
        if delta == 0:
            hue = 0
        elif max_value == r:
            hue = (60 * ((g - b) // delta)) % 360
        elif max_value == g:
            hue = (60 * ((b - r) // delta) + 120) % 360
        else:
            hue = (60 * ((r - g) // delta) + 240) % 360

        # Compute the saturation value
        if max_value == 0:
            saturation = 0
        else:
            saturation = (delta * 100) // max_value

        # Compute the value (brightness) value
        value = (max_value * 100) // 255

        return (hue, saturation, value)


    def hsv_to_rgb(hue, saturation, value):

        # Compute the chroma value
        chroma = (value * saturation) // 100

        # Compute the intermediate values
        hue_prime = hue // 60
        x = chroma * (100 - abs((hue % 60) - 30)) // 100

        # Compute the RGB values based on the hue region
        if hue_prime == 0:
            r, g, b = chroma, x, 0
        elif hue_prime == 1:
            r, g, b = x, chroma, 0
        elif hue_prime == 2:
            r, g, b = 0, chroma, x
        elif hue_prime == 3:
            r, g, b = 0, x, chroma
        elif hue_prime == 4:
            r, g, b = x, 0, chroma
        else:
            r, g, b = chroma, 0, x

        # Compute the lightness value
        lightness = value - chroma

        # Adjust RGB values by adding lightness
        r = (r + lightness) * 255 // 100
        g = (g + lightness) * 255 // 100
        b = (b + lightness) * 255 // 100

        return (r, g, b)