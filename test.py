import colorsys
import logging, os, argparse, colorsys
from time import perf_counter_ns
from convert import ChatGPT, Helper, Chernov

def init_logger(logfile):
    if os.path.exists(logfile): os.remove(logfile)
    logger = logging.getLogger(logfile)
    handler = logging.StreamHandler(open(logfile, "w", encoding="utf-8"))
    handler.setFormatter(logging.Formatter('DEBUG: $message', style='$'))
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


def test_correctness_rgb(rgb: tuple[int, int, int]):
    rgb_decimal = tuple([i / 255 for i in rgb])
    hsv_colorsys = colorsys.rgb_to_hsv(*rgb_decimal)

    if args.chatgpt:
        s1 = ChatGPT.rgb_to_hsv(*rgb)
    elif args.colorsys:
        s1 = tuple( [round(i * 255) for i in colorsys.hsv_to_rgb(*hsv_colorsys)] )
    
    control = ( int(hsv_colorsys[0] * 360), int(hsv_colorsys[1] * 100), int(hsv_colorsys[2] * 100) )
    s2 = Chernov.rgb_to_hsv_degrees(*rgb)

    try:
        if args.colorsys:
            assert(rgb == s1)
        elif args.chatgpt:
            assert(control == s1)
        else:
            assert(control == s2)
    except AssertionError:
        e = True
        o = f'\n\t{"✓" if control == s1 else "⚠"} ChatGPT\t{s1}' if args.chatgpt else ''
        print(f"""{"⚠" if e else "✓"} \x1b[48;2;{rgb[0]};{rgb[1]};{rgb[2]}m#{Helper.rgb_to_hex(rgb)}\x1b[0m {rgb}
\tCONTROL {"HSV" if not args.colorsys else "RGB"}\t{ control if not args.colorsys else s1 }{o}
\t{"✓" if control == s2 else "⚠"} Chernov\t{s2}""")


if __name__ == '__main__':
   
    parser = argparse.ArgumentParser(
        prog='Fast color conversion test',
        description='Testing color conversion. If the methods fail, the error messages appear in .log files.')

    parser.add_argument('command', choices=['time','correctness'] )
    parser.add_argument('--all', action='store_true', help='Test all possible RGB colors: 2^8 * 2^8 * 2^8')
    parser.add_argument('--chatgpt', action='store_true', help='Test ChatGPT functions')
    parser.add_argument('--colorsys', action='store_true', help='Test colorsys functions')
    parser.add_argument('--log', default='color_conversion_test.log', help='Where to log errors')
    
    args = parser.parse_args()

    if args.command == 'correctness':
        cont = input('WARNING: This test shows many flashing colors. Continue? [y/n]: ').lower().strip() == 'y'
        if not cont: exit(0)

    if args.all:
        print('INFO: Setting up large data...')
        rgb_sample_g = range(0xFFFFFF+1)
    else:
        rgb_sample_g = [0x000000, 0xFFFFFF, 0x8b4a8b, 0xd59cd5, 0x4a2941, 0xeedeb4, 0xa47308, 0xb473bd]
    
    rgb_sample = list(map(Helper.hex_to_rgb, rgb_sample_g))

    print('INFO: Timing start')
    t1 = perf_counter_ns()

    if args.command == 'correctness':
        for v in rgb_sample:
            test_correctness_rgb(v)

    elif args.command == 'time':
        
        if args.chatgpt:
            t_1 = perf_counter_ns()
            for v in rgb_sample: ChatGPT.rgb_to_hsv(*v)
            t_2 = perf_counter_ns()
        
        t_3 = perf_counter_ns()
        for v in rgb_sample: Chernov.rgb_to_hsv(*v)
        t_4 = perf_counter_ns()

        t_5 = perf_counter_ns()
        for v in rgb_sample: colorsys.rgb_to_hsv(*v)
        t_6 = perf_counter_ns()

        print(f"""RGB to HSV Timing test complete.
    Colorsys.rgb_to_hsv(): {(t_6 - t_5) // 1000000} ms
    {f"ChatGPT.rgb_to_hsv(): {(t_2 - t_1) // 1000000} ms" if args.chatgpt else ""}
    Chernov.rgb_to_hsv(): {(t_4 - t_3) // 1000000} ms
""")
        if args.chatgpt:
            t_1 = perf_counter_ns()
            for v in rgb_sample: ChatGPT.hsv_to_rgb(*v)
            t_2 = perf_counter_ns()
        
        t_3 = perf_counter_ns()
        for v in rgb_sample: Chernov.hsv_to_rgb(*v)
        t_4 = perf_counter_ns()

        t_5 = perf_counter_ns()
        for v in rgb_sample: colorsys.hsv_to_rgb(*v)
        t_6 = perf_counter_ns()

        print(f"""HSV to RGB Timing test complete.
    Colorsys.hsv_to_rgb(): {(t_6 - t_5) // 1000000} ms
    {f"ChatGPT.hsv_to_rgb(): {(t_2 - t_1) // 1000000} ms" if args.chatgpt else ""}
    Chernov.hsv_to_rgb(): {(t_4 - t_3) // 1000000} ms
""")

    t2 = perf_counter_ns()
    
    print(f'INFO: Elapsed time: { (t2 - t1) // 1000000 } ms')
